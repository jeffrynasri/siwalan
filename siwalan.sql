-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2021 at 12:48 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siwalan`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `timestamp` int(10) UNSIGNED DEFAULT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('at7brk6gihe7pd9frin5qlmgo0ffoi98', '::1', 1639565545, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393536353534353b),
('pfuik2kvd1d6mksadks8l9chhlkrn3to', '::1', 1639565478, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393536353234343b),
('olj53q7m8ehk8dtka5emsei7m25pbir6', '::1', 1639565880, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393536353838303b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a303a22223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('c1hn0gpacqs9n2mv8tb1i8ks3ifvl7js', '::1', 1639566228, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393536363232383b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a303a22223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('ebhrfksr601thbn9r2t6r0bbpq2gguo6', '::1', 1639567302, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393536373330323b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a303a22223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('q68jtelhul9muj3so3te7vio9itm1nd5', '::1', 1639568288, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393536383238383b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a303a22223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('2h85pet49vjr5h9h02ib33qmp9sa3id4', '::1', 1639568288, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393536383238383b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a31303a22737570657261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a303a22223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('8k3d73iv698h49os56fpich25t5pqsqf', '::1', 1639654360, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393635343336303b),
('hdt92qftp7o0amkoe0voi1c2pefdjvq1', '::1', 1639654680, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393635343638303b),
('chftbqab842ttcvv43f06b8m86q843sp', '::1', 1639655212, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393635343933373b);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id` varchar(32) NOT NULL,
  `nama` varchar(99) NOT NULL,
  `tempat` varchar(99) NOT NULL,
  `keterangan` varchar(128) NOT NULL,
  `waktu` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_pimpinan`
--

CREATE TABLE `jadwal_pimpinan` (
  `id_jadwal` varchar(32) NOT NULL,
  `id_pimpinan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `loguser`
--

CREATE TABLE `loguser` (
  `logid` int(11) NOT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `logdate` datetime DEFAULT NULL,
  `logact` varchar(11) DEFAULT NULL,
  `logip` varchar(15) NOT NULL,
  `logplatagent` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loguser`
--

INSERT INTO `loguser` (`logid`, `userid`, `logdate`, `logact`, `logip`, `logplatagent`) VALUES
(1, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-12-15 17:52:28', 'login', '::1', '(Windows 10) Opera 82.0.4227.23'),
(2, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-12-16 18:38:34', 'login', '::1', '(Windows 10) Opera 82.0.4227.23'),
(3, NULL, '2021-12-16 18:42:17', 'logout', '::1', '(Windows 10) Opera 82.0.4227.23');

-- --------------------------------------------------------

--
-- Table structure for table `pimpinan`
--

CREATE TABLE `pimpinan` (
  `userid` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `deskripsi` varchar(99) NOT NULL,
  `ustate` tinyint(1) NOT NULL,
  `ucreated` datetime NOT NULL,
  `umodified` datetime NOT NULL,
  `udeleted` datetime NOT NULL,
  `ucreatedby` varchar(100) NOT NULL,
  `umodifiedby` varchar(100) NOT NULL,
  `udeletedby` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pimpinan`
--

INSERT INTO `pimpinan` (`userid`, `nama`, `email`, `pass`, `uname`, `deskripsi`, `ustate`, `ucreated`, `umodified`, `udeleted`, `ucreatedby`, `umodifiedby`, `udeletedby`) VALUES
('602d39f500e949728206522441de58ce', 'BUPATI', '', '$2y$16$dQyznTm2OoJOaGCmz/4pF.DMTt37hC9TRINbqK5VPFbtNW0U.olzG', 'bupati', '', 1, '2021-12-15 11:59:06', '2021-12-15 11:59:06', '0000-00-00 00:00:00', 'superadmin', 'superadmin', ''),
('bb2438dbffb14957b5ad6805d6420f30', 'WAKIL BUPATI', '', '$2y$16$Em5Oy438cR0PI743lr59bOJhtk0i7/BFOMmKDOz0y3ybZDYGJiS3a', 'wabup', '', 1, '2021-12-15 11:59:34', '2021-12-15 11:59:34', '0000-00-00 00:00:00', 'superadmin', 'superadmin', '');

-- --------------------------------------------------------

--
-- Table structure for table `superadmin`
--

CREATE TABLE `superadmin` (
  `userid` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `deskripsi` varchar(99) NOT NULL,
  `ustate` tinyint(1) NOT NULL,
  `ucreated` datetime NOT NULL,
  `umodified` datetime NOT NULL,
  `udeleted` datetime NOT NULL,
  `ucreatedby` varchar(100) NOT NULL,
  `umodifiedby` varchar(100) NOT NULL,
  `udeletedby` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `superadmin`
--

INSERT INTO `superadmin` (`userid`, `nama`, `email`, `pass`, `uname`, `deskripsi`, `ustate`, `ucreated`, `umodified`, `udeleted`, `ucreatedby`, `umodifiedby`, `udeletedby`) VALUES
('19987a33abde48329d928d7681320d0a', 'Sekpri Bupati 1', '', '$2y$16$RZ/3478x1bu4xdWqQP9Nne.dtmQqZYq27.hQ7saTsVAsUIgmA0mA6', 'sekpri_bupati1', '', 1, '2021-12-16 18:46:38', '2021-12-16 18:46:38', '0000-00-00 00:00:00', 'Hulk', 'Hulk', ''),
('362bb7472157481bb7209a3188583ce5', 'Sekpri Bupati 2', '', '$2y$16$DChq/ypA7ExgsQjEKUEvceqLSUzRKfcOKkFwnHoZjtfqkSHmKOyIa', 'sekpri_bupati2', '', 1, '2021-12-16 18:46:43', '2021-12-16 18:46:43', '0000-00-00 00:00:00', 'Hulk', 'Hulk', ''),
('6ca50e4f150e4e6190b28bd9bf0cd15c', '', '', '$2y$10$x3axrUu07EcIC3.VOcMt6Omlgd9XYEHLGMPtGWYXtoJ/Z03gpVBlW', 'superadmin', 'superadmin', 1, '2020-12-13 14:19:16', '2020-12-13 14:19:16', '0000-00-00 00:00:00', '', '', ''),
('c5804d3f9e984189a0d91d50abef83e3', 'Sekpri Wakil Bupati 2', '', '$2y$16$k8K.WHsZBQwV9IqlbF662e8fUaCC.zS2gFj02q.AelpegniS3i/TS', 'sekpri_wabup2', '', 1, '2021-12-16 18:46:52', '2021-12-16 18:46:52', '0000-00-00 00:00:00', 'Hulk', 'Hulk', ''),
('e866e1c4fd254e9e9bc8e526c476b7d2', 'Sekpri Wakil Bupati 1', '', '$2y$16$wDWoWWCD64bpcigKl5eY.ed/vyzi14pN4bzW6wHMmVF2YhTIDjYEy', 'sekpri_wabup1', '', 1, '2021-12-16 18:46:47', '2021-12-16 18:46:47', '0000-00-00 00:00:00', 'Hulk', 'Hulk', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `deskripsi` varchar(99) NOT NULL,
  `ustate` tinyint(1) NOT NULL,
  `ucreated` datetime NOT NULL,
  `umodified` datetime NOT NULL,
  `udeleted` datetime NOT NULL,
  `ucreatedby` varchar(100) NOT NULL,
  `umodifiedby` varchar(100) NOT NULL,
  `udeletedby` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_menghadiri`
--

CREATE TABLE `user_menghadiri` (
  `id_user` varchar(32) DEFAULT NULL,
  `id_jadwal` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_pimpinan`
--
ALTER TABLE `jadwal_pimpinan`
  ADD KEY `fk_jadwal` (`id_jadwal`),
  ADD KEY `fk_pimpinan` (`id_pimpinan`);

--
-- Indexes for table `loguser`
--
ALTER TABLE `loguser`
  ADD PRIMARY KEY (`logid`),
  ADD KEY `fk_loguser` (`userid`);

--
-- Indexes for table `pimpinan`
--
ALTER TABLE `pimpinan`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `superadmin`
--
ALTER TABLE `superadmin`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `user_menghadiri`
--
ALTER TABLE `user_menghadiri`
  ADD KEY `fk_user` (`id_user`),
  ADD KEY `fk_jadwall` (`id_jadwal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loguser`
--
ALTER TABLE `loguser`
  MODIFY `logid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal_pimpinan`
--
ALTER TABLE `jadwal_pimpinan`
  ADD CONSTRAINT `fk_jadwal` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id`),
  ADD CONSTRAINT `fk_pimpinan` FOREIGN KEY (`id_pimpinan`) REFERENCES `pimpinan` (`userid`);

--
-- Constraints for table `user_menghadiri`
--
ALTER TABLE `user_menghadiri`
  ADD CONSTRAINT `fk_jadwall` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id`),
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`userid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
