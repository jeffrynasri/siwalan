<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//PRINT BULAN DALAM FORMAT INDONESIA
if (!function_exists('bulan_indo'))
{
	function bulan_indo($number,$format){
				$return="";
        if($format=='m'){
					if((int)$number==1){
						$return='Jan';
					}else if((int)$number==2){
						$return='Feb';
					}else if((int)$number==3){
						$return='Mar';
					}else if((int)$number==4){
						$return='Apr';
					}else if((int)$number==5){
						$return='Mei';
					}else if((int)$number==6){
						$return='Jun';
					}else if((int)$number==7){
						$return='Jul';
					}else if((int)$number==8){
						$return='Agu';
					}else if((int)$number==9){
						$return='Sep';
					}else if((int)$number==10){
						$return='Okt';
					}else if((int)$number==11){
						$return='Nov';
					}else if((int)$number==12){
						$return='Des';
					}
				}else if($format=='F'){
					if((int)$number==1){
						$return='Januari';
					}else if((int)$number==2){
						$return='Februari';
					}else if((int)$number==3){
						$return='Maret';
					}else if((int)$number==4){
						$return='April';
					}else if((int)$number==5){
						$return='Mei';
					}else if((int)$number==6){
						$return='Juni';
					}else if((int)$number==7){
						$return='Juli';
					}else if((int)$number==8){
						$return='Agustus';
					}else if((int)$number==9){
						$return='September';
					}else if((int)$number==10){
						$return='Oktober';
					}else if((int)$number==11){
						$return='November';
					}else if((int)$number==12){
						$return='Desember';
					}
				}

				return $return;
    }

}

//HARI INDO
if(!function_exists('hari_indo')){
	function hari_indo($number,$format){
		$return='';
		if($format=='D'){
			if($number==1){
				$return = 'Sen';
			}else if($number==2){
				$return = 'Sel';
			}else if($number==3){
				$return = 'Rab';
			}else if($number==4){
				$return = 'Kam';
			}else if($number==5){
				$return = 'Jum';
			}else if($number==6){
				$return = 'Sab';
			}else if($number==7){
				$return = 'Min';
			}
		}else if($format=='l'){
			if($number==1){
				$return = 'Senin';
			}else if($number==2){
				$return = 'Selasa';
			}else if($number==3){
				$return = 'Rabu';
			}else if($number==4){
				$return = 'Kamis';
			}else if($number==5){
				$return = 'Jumat';
			}else if($number==6){
				$return = 'Sabtu';
			}else if($number==7){
				$return = 'Minggu';
			}
		}
		return $return;
	}
}

//Mengurangi 2 tanggal, hasilnya dalam menit
if(!function_exists('selisih_tanggal')){
    function selisih_tanggal($date1,$date2){
        return abs(( strtotime($date2) - strtotime($date1))/60) ;
    }
}
