<?php
include_once APPPATH .'/core/Admin_controller.php';

class Admin_dashboard extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Jadwal_model");
        $this->load->model("Jadwal_pimpinan_model");
        $this->load->model("User_model");
        $this->load->model("User_menghadiri_model");
    }
    function get_jadwal_mingguini(){
        $jadwal_minggu_ini = $this->Jadwal_model->get_all_jadwal(array(
            'WEEKOFYEAR(waktu)' => date('W')
        ),'waktu');
        $output=array();
        foreach($jadwal_minggu_ini as $jdm){
            $tanggal = date('D, d/m/Y',strtotime($jdm['waktu']));
            if(!isset($output[$tanggal])){
                $output[$tanggal]=array();
            }
            array_push($output[$tanggal],$jdm);
        }
        return $output;
    }
    function index()
    {
        $data['all_jadwal']=$this->Jadwal_model->get_all_jadwal();
        $data['jadwal_hariini']=$this->Jadwal_model->get_all_jadwal(array(
            'DAY(waktu)' => date('d'),
            'month(waktu)' => date('m'),
            'YEAR(waktu)' => date('Y')
        ),'waktu');
        $data['jadwal_mingguini']=$this->get_jadwal_mingguini();
        $data['_view'] = 'dashboard';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
    }
    function get_data_jadwal_json()
    {
        $list = $this->Jadwal_model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date_format(new DateTime($field->waktu),"D, d/m/Y");
            $row[] = date_format(new DateTime($field->waktu),"H:i");
            $row[] = $field->nama;
            $row[] = $field->tempat;

            $temp="<ul>";
            $pimpinan_selected=$this->Jadwal_pimpinan_model->get_all_jadwal_pimpinan(array(
                'id_jadwal'=>$field->id
            ));
            foreach($pimpinan_selected as $pim_selected){
                $temp = $temp . "<li>". $pim_selected['pimpinan_nama'] . "</li>";
            }
            $temp=$temp . "</ul>";
            $row[] = $temp;

            $temp="<ul>";
            $user_selected=$this->User_menghadiri_model->get_all_user_menghadiri(array(
                'id_jadwal'=>$field->id
            ));
            foreach($user_selected as $usr_selected){
                $temp = $temp . "<li>". $usr_selected['user_nama'] . "</li>";
            }
            $temp=$temp . "</ul>";
            $row[] = $temp;
            
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Jadwal_model->count_all(),
            "recordsFiltered" => $this->Jadwal_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}

