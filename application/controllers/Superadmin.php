<?php

include_once APPPATH. '/core/Admin_controller.php';
class Superadmin extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Superadmin_model');
        $this->load->helper('password_helper');
    } 

    /*
     * Listing of superadmin
     */
    function index()
    {
        $data['_view'] = 'superadmin/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
    }

    /*
     * Adding a new superadmin
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('uname','Uname','required');
		$this->form_validation->set_rules('pass','Pass','required');
		$this->form_validation->set_rules('nama','Nama','required');
		
		if($this->form_validation->run())     
        {   
            $params = array(
				'superadminid' => str_replace("-","",$this->uuid->v4()),
                'nama' => $this->input->post('nama'),
                'email' => $this->input->post('email'),
                'pass' => hash_password($this->input->post('pass')),
                'uname' => $this->input->post('uname'),
                'deskripsi' => $this->input->post('deskripsi'),
                'ustate' => $this->input->post('ustate'),
                'ucreated' => date('Y-m-d H:i:s'),
                'umodified' => date('Y-m-d H:i:s'),
                'ucreatedby' => $this->session->superadmindata(SESSION_LOGIN_USERNAME),
                'umodifiedby' => $this->session->superadmindata(SESSION_LOGIN_USERNAME),
            );
            
            $superadmin_id = $this->Superadmin_model->add_superadmin($params);
            redirect('superadmin/index');
        }
        else
        {            
            $data['_view'] = 'superadmin/add';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
    }  

    /*
     * Editing a superadmin
     */
    function edit($id)
    {   
        // check if the superadmin exists before trying to edit it
        $data['superadmin'] = $this->Superadmin_model->get_superadmin($id);
        
        if(isset($data['superadmin']['superadmin']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('uname','Uname','required');
			$this->form_validation->set_rules('nama','Nama','required');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'nama' => $this->input->post('nama'),
                    'email' => $this->input->post('email'),
                    'uname' => $this->input->post('uname'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'ustate' => $this->input->post('ustate'),
                    'umodified' => date('Y-m-d H:i:s'),
                    'umodifiedby' => $this->session->superadmindata(SESSION_LOGIN_USERNAME),
                );

                $this->Superadmin_model->update_superadmin($id,$params);            
                redirect('superadmin/index');
            }
            else
            {
                $data['_view'] = 'superadmin/edit';
                $data['_header'] = 'layouts/admin_header';
                $data['_sidebar'] = 'layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The superadmin you are trying to edit does not exist.');
    } 

    /*
     * Deleting superadmin
     */
    function remove($superadminid)
    {
        $superadmin = $this->Superadmin_model->get_superadmin($superadminid);

        // check if the superadmin exists before trying to delete it
        if(isset($superadmin['superadmin']))
        {
            $this->Superadmin_model->delete_superadmin($superadminid);
            redirect('superadmin/index');
        }
        else
            show_error('The superadmin you are trying to delete does not exist.');
    }
    function get_data_superadmin_json()
    {

        $list = $this->Superadmin_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row[] = $field->nama;
            $row[] = $field->uname;
            $row[] = $field->deskripsi;
            if($field->ustate=='1'){
                $row[] = "Aktif";
            }else{
                $row[] = "Tidak Aktif";
            }

            $row[] = "<a href='" .  'edit/'. $field->superadminid ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->superadminid ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Superadmin_model->count_all(),
            "recordsFiltered" => $this->Superadmin_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
