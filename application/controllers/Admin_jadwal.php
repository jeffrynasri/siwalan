<?php

include_once APPPATH . '/core/Admin_controller.php';
class Admin_jadwal extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jadwal_model');
        $this->load->model('Jadwal_pimpinan_model');
        $this->load->model('User_menghadiri_model');
        $this->load->model('Pimpinan_model');
        $this->load->model('User_model');
    } 
    function is_jadwal_empty_versiawal($waktu_awal,$waktu_akhir,$id_pimpinan){
        
        $blocks = $this->Jadwal_pimpinan_model->get_all_jadwal_pimpinan(array(
            'id_pimpinan' => $id_pimpinan,
            
        ));
        // GAWE HMTC OPTIMASI
        //'DATE("jadwal.waktu_akhir")' => array(date_format($waktu_awal,'d'),date_format($waktu_akhir,'d')),
        // 'MONTH("jadwal.waktu_akhir")' => array(date_format($waktu_awal,'m'),date_format($waktu_akhir,'m')),
        // 'YEAR("jadwal.waktu_akhir")' => array(date_format($waktu_awal,'Y'),date_format($waktu_akhir,'Y')),
        $overlaps = 0;
        foreach ($blocks as $block) {
            if ($waktu_awal < $block['waktu_akhir'] && $waktu_akhir > $block['waktu_awal']) {
                ++$overlaps;
            }
        }
        return $overlaps;
        // if $overlaps > 0 -> there's a booking in the way
    }
    function is_jadwal_empty($waktu,$id_pimpinan){
        
        $blocks = $this->Jadwal_pimpinan_model->get_all_jadwal_pimpinan(array(
            'id_pimpinan' => $id_pimpinan,
            'DAY(jadwal.waktu)' => date('d',strtotime($waktu)),
            'MONTH(jadwal.waktu)' => date('m',strtotime($waktu)),
            'YEAR(jadwal.waktu)' => date('Y',strtotime($waktu)),
                
        ));
        foreach ($blocks as $block) {
            $waktu_time = date('Y-m-d H:i:s',strtotime($waktu));
            $block_time = date('Y-m-d H:i:s',strtotime($block['waktu']));
            if ($waktu_time == $block_time) {
                return false;
            }
        }
        return true;
    }

    
    /*
     * Listing of admin_jadwal
     */
    function index()
    {
        $data['all_pimpinan']=$this->Pimpinan_model->get_all_pimpinan();
        $data['_view'] = 'jadwal/admin/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';

        $data['_view'] = 'jadwal/admin/index';
        $this->load->view('layouts/admin_template',$data);
    }
    function add()
    {
        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('waktu','Waktu','required');
        $this->form_validation->set_rules("tempat","Tempat",'required');
        $this->form_validation->set_rules("pimpinan[]","Pimpinan",'required');
        $this->form_validation->set_rules("user[]","OPD",'required');
        $data['all_pimpinan']=$this->Pimpinan_model->get_all_pimpinan();
        $data['all_user']=$this->User_model->get_all_user();
        if($this->form_validation->run())
        {
            $pimpinan=array();
            $pimpinan = $_POST['pimpinan'];
            $user=array();
            $user = $_POST['user'];
            //PENGECEKAN JADWAL SEMUA PIMPINAN
            foreach($pimpinan as $pim){
                if(!$this->is_jadwal_empty($this->input->post('waktu'),$pim)){
                    //show_error($pimpinan_satuan['nama'] . " Sudah Ada Kegiatan");
                    $pimpinan_satuan=$this->Pimpinan_model->get_pimpinan($pim);
                    $this->session->set_flashdata('error',$pimpinan_satuan['nama'] . " Sudah Ada Kegiatan");
                    redirect('admin_jadwal/index');
                }
            }
            
            //JIKA JADWAL SEMUA AMAN
            $id_jadwal = str_replace("-","",$this->uuid->v4());            
            $params = array(
                'id' => $id_jadwal,
                'nama' => $this->input->post('nama'),
                'tempat' => $this->input->post('tempat'),
                'waktu' => $this->input->post('waktu'),
                'keterangan' => $this->input->post('keterangan'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $this->Jadwal_model->add_jadwal_new($params);
            foreach($pimpinan as $pim){
                $this->Jadwal_pimpinan_model->add_jadwal_pimpinan_new(array(
                    'id_jadwal'=> $id_jadwal,
                    'id_pimpinan'=> $pim,
                ));
            }
            foreach($user as $usr){
                $this->User_menghadiri_model->add_user_menghadiri_new(array(
                    'id_jadwal'=> $id_jadwal,
                    'id_user'=> $usr,
                )); 
            }
           redirect('admin_jadwal/index');
        }
    
        $data['_view'] = 'jadwal/admin/add';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar']='layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
      
    }
  
  
    /*
     * Editing a jadwal
     */
    function edit($id)
    {   
        // check if the jadwal exists before trying to edit it
        $data['jadwal'] = $this->Jadwal_model->get_jadwal($id);
        
        if(isset($data['jadwal']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nama','Nama','required');
            $this->form_validation->set_rules('waktu','Waktu','required');
            $this->form_validation->set_rules("tempat","Tempat",'required');
            $this->form_validation->set_rules("pimpinan[]","Pimpinan",'required');
            $this->form_validation->set_rules("user[]","OPD",'required');
		
			if($this->form_validation->run())     
            {   
                $pimpinan=array();
                $pimpinan = $_POST['pimpinan'];
                $user=array();
                $user = $_POST['user'];
                //DATA JADWAL
                $params = array(
                    'nama' => $this->input->post('nama'),
                    'tempat' => $this->input->post('tempat'),
                    'waktu' => $this->input->post('waktu'),
                    'keterangan' => $this->input->post('keterangan'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                $this->Jadwal_model->update_jadwal($id,$params);       
                
                //DATA JADWAL PIMPINAN
                $this->Jadwal_pimpinan_model->delete_jadwal_pimpinan(array(
                    'id_jadwal' => $id
                ));

                foreach($pimpinan as $pim){
                    $this->Jadwal_pimpinan_model->add_jadwal_pimpinan_new(array(
                        'id_jadwal'=> $id,
                        'id_pimpinan'=> $pim,
                    ));
                }

                //DATA USER MENGHADIRI
                $this->User_menghadiri_model->delete_user_menghadiri(array(
                    'id_jadwal' => $id
                ));

                foreach($user as $usr){
                    $this->User_menghadiri_model->add_user_menghadiri_new(array(
                        'id_jadwal'=> $id,
                        'id_user'=> $usr,
                    )); 
                }

                redirect('admin_jadwal/index');
            }
            else
            {
                $data['all_pimpinan']=$this->Pimpinan_model->get_all_pimpinan();
                $data['all_user']=$this->User_model->get_all_user();
                //GET PIMPINAN YANG DI SELECET
                $array_pimpinan_selected=$this->Jadwal_pimpinan_model->get_all_jadwal_pimpinan(array(
                    'id_jadwal' =>  $data['jadwal']['id']
                ));
                $data['array_pimpinan_selected']=array();
                foreach($array_pimpinan_selected as $pimpinan_selected){
                    array_push($data['array_pimpinan_selected'],$pimpinan_selected['id_pimpinan']);
                }
                //GET USER YANG DI SELECET
                $array_user_selected=$this->User_menghadiri_model->get_all_user_menghadiri(array(
                    'id_jadwal' =>  $data['jadwal']['id']
                ));
                $data['array_user_selected']=array();
                foreach($array_user_selected as $user_selected){
                    array_push($data['array_user_selected'],$user_selected['id_user']);
                }
                $data['_view'] = 'jadwal/admin/edit';
                $data['_header']='layouts/admin_header';
                $data['_sidebar']='layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The jadwal you are trying to edit does not exist.');
    }
   

    /*
     * Deleting jadwal
     */
    function remove($id)
    {
        $jadwal = $this->Jadwal_model->get_jadwal($id);

        // check if the jadwal exists before trying to delete it
        if(isset($jadwal['id']))
        {
            //DATA JADWAL PIMPINAN
            $this->Jadwal_pimpinan_model->delete_jadwal_pimpinan(array(
                'id_jadwal' => $id
            ));
            //DATA USER MENGHADIRI
            $this->User_menghadiri_model->delete_user_menghadiri(array(
                'id_jadwal' => $id
            ));

            $this->Jadwal_model->delete_jadwal($id);
            redirect('admin_jadwal/index');
        }
        else
            show_error('The jadwal you are trying to delete does not exist.');
    }

    function get_data_jadwal_json()
    {
        $waktu_explode=explode("-",$this->input->post('waktu'));
        $params_where=array(
            "day(waktu)"=> $waktu_explode[2],
            "month(waktu)"=> $waktu_explode[1],
            "year(waktu)"=> $waktu_explode[0]
        );
        // if($this->input->post('id_pimpinan')!='-1'){
        //     $params_where['userid']= $this->input->post('id_pimpinan');
        // }

        $list = $this->Jadwal_model->get_datatables($params_where);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date_format(new DateTime($field->waktu),"D, d/m/Y");
            $row[] = date_format(new DateTime($field->waktu),"H:i");
            $row[] = $field->nama;
            $row[] = $field->tempat;
            $row[] = $field->keterangan;
            $temp="<ul>";
            $pimpinan_selected=$this->Jadwal_pimpinan_model->get_all_jadwal_pimpinan(array(
                'id_jadwal'=>$field->id
            ));
            foreach($pimpinan_selected as $pim_selected){
                $temp = $temp . "<li>". $pim_selected['pimpinan_nama'] . "</li>";
            }
            $temp=$temp . "</ul>";
            $row[] = $temp;

            $temp="<ul>";
            $user_selected=$this->User_menghadiri_model->get_all_user_menghadiri(array(
                'id_jadwal'=>$field->id
            ));
            foreach($user_selected as $usr_selected){
                $temp = $temp . "<li>". $usr_selected['user_nama'] . "</li>";
            }
            $temp=$temp . "</ul>";
            $row[] = $temp;
            $row[] = "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>"
            ."<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";
            
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Jadwal_model->count_all($params_where),
            "recordsFiltered" => $this->Jadwal_model->count_filtered($params_where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
