<?php

include_once APPPATH . '/core/Admin_controller.php';
class User_jadwal extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jadwal_model');
        $this->load->model('Jadwal_pimpinan_model');
        $this->load->model('User_menghadiri_model');
        $this->load->model('Pimpinan_model');
        $this->load->model('User_model');
    } 
    

    
    /*
     * Listing of admin_jadwal
     */
    function index()
    {
        $data['all_pimpinan']=$this->Pimpinan_model->get_all_pimpinan();
        $data['_view'] = 'jadwal/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/user_sidebar';

        $this->load->view('layouts/admin_template',$data);
    }
    
    function get_data_jadwal_json()
    {
        $waktu_explode=explode("-",$this->input->post('waktu'));
        $params_where=array(
            "day(waktu)"=> $waktu_explode[2],
            "month(waktu)"=> $waktu_explode[1],
            "year(waktu)"=> $waktu_explode[0]
        );
        
        $params_where['id_user']=$this->session->userdata(SESSION_LOGIN_USERID);
        $list = $this->User_menghadiri_model->get_datatables($params_where);
        // if($this->input->post('id_pimpinan')!='-1'){
        //     $params_where['userid']= $this->input->post('id_pimpinan');
        // }
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date_format(new DateTime($field->waktu),"D, d/m/Y");
            $row[] = date_format(new DateTime($field->waktu),"H:i");
            $row[] = $field->nama;
            $row[] = $field->tempat;
            $row[] = $field->keterangan;
            $temp="<ul>";
            $pimpinan_selected=$this->Jadwal_pimpinan_model->get_all_jadwal_pimpinan(array(
                'id_jadwal'=>$field->id_jadwal
            ));
            foreach($pimpinan_selected as $pim_selected){
                $temp = $temp . "<li>". $pim_selected['pimpinan_nama'] . "</li>";
            }
            $temp=$temp . "</ul>";
            $row[] = $temp;
            //$row[] = "asdasd";
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->User_menghadiri_model->count_all($params_where),
            "recordsFiltered" => $this->User_menghadiri_model->count_filtered($params_where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
