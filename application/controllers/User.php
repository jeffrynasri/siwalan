<?php

include_once APPPATH. '/core/Admin_controller.php';
class User extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Pimpinan_model');
        $this->load->model('Superadmin_model');
        $this->load->helper('password_helper');
    } 
    function is_username_exist($username){
        $user = $this->User_model->get_user_byusername($username);
		$pimpinan = $this->Pimpinan_model->get_pimpinan_byusername($username);
		$superadmin = $this->Superadmin_model->get_superadmin_byusername($username);
        if($user || $pimpinan || $superadmin){
            return true;
        }
        return false;
    }
    /*
     * Listing of user
     */
    function index()
    {
        $data['_view'] = 'user/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
    }

    /*
     * Adding a new user
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('uname','Uname','required');
		$this->form_validation->set_rules('pass','Pass','required');
		$this->form_validation->set_rules('nama','Nama','required');
		
		if($this->form_validation->run())     
        {   
            if($this->is_username_exist($this->input->post('uname'))){
                show_error("USername Sudah Ada");
            }
            $params = array(
				'userid' => str_replace("-","",$this->uuid->v4()),
                'nama' => $this->input->post('nama'),
                'email' => $this->input->post('email'),
                'pass' => hash_password($this->input->post('pass')),
                'uname' => $this->input->post('uname'),
                'deskripsi' => $this->input->post('deskripsi'),
                'ustate' => $this->input->post('ustate'),
                'ucreated' => date('Y-m-d H:i:s'),
                'umodified' => date('Y-m-d H:i:s'),
                'ucreatedby' => $this->session->userdata(SESSION_LOGIN_USERNAME),
                'umodifiedby' => $this->session->userdata(SESSION_LOGIN_USERNAME),
            );
            
            $user_id = $this->User_model->add_user($params);
            redirect('user/index');
        }
        else
        {            
            $data['_view'] = 'user/add';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
    }  

    /*
     * Editing a user
     */
    function edit($id)
    {   
        // check if the user exists before trying to edit it
        $data['user'] = $this->User_model->get_user($id);
        
        if(isset($data['user']['userid']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('uname','Uname','required');
			$this->form_validation->set_rules('nama','Nama','required');
		
			if($this->form_validation->run())     
            {   
                if($this->input->post('uname_old')!=$this->input->post('uname')){
                    if($this->is_username_exist($this->input->post('uname'))){
                        show_error('Username sudah ada');
                    }
                }
                $params = array(
					'nama' => $this->input->post('nama'),
                    'email' => $this->input->post('email'),
                    'uname' => $this->input->post('uname'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'ustate' => $this->input->post('ustate'),
                    'umodified' => date('Y-m-d H:i:s'),
                    'umodifiedby' => $this->session->userdata(SESSION_LOGIN_USERNAME),
                );
                if($this->input->post('password')){
                    $params['pass']=hash_password($this->input->post('pass'));
                }
                $this->User_model->update_user($id,$params);            
                redirect('user/index');
            }
            else
            {
                $data['_view'] = 'user/edit';
                $data['_header'] = 'layouts/admin_header';
                $data['_sidebar'] = 'layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The user you are trying to edit does not exist.');
    } 

    /*
     * Deleting user
     */
    function remove($userid)
    {
        $user = $this->User_model->get_user($userid);

        // check if the user exists before trying to delete it
        if(isset($user['userid']))
        {
            $this->User_model->delete_user($userid);
            redirect('user/index');
        }
        else
            show_error('The user you are trying to delete does not exist.');
    }
    function get_data_user_json()
    {

        $list = $this->User_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row=array();
            $row[] = $field->nama;
            $row[] = $field->uname;
            $row[] = $field->deskripsi;
            if($field->ustate=='1'){
                $row[] = "Aktif";
            }else{
                $row[] = "Tidak Aktif";
            }

            $row[] = "<a href='" .  'edit/'. $field->userid ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->userid ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->User_model->count_all(),
            "recordsFiltered" => $this->User_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
