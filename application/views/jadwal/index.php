<?php date_default_timezone_set("Asia/Bangkok"); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Jadwal</h3>
            	<div class="box-tools">
                   
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                      <span>Tanggal</span>
                      <input type="date" name="waktu" id="waktu" value="<?php echo date('Y-m-d'); ?>"/>
                  </div>
                  <!-- <div class="col-md-12">
                      <span>Pimpinan</span>
                      <select name='id_pimpinan' id="id_pimpinan">
                        <option value='-1' >Semua<option>
                        <?php foreach($all_pimpinan as $pimpinan){ ?>
                          <option value='<?php echo $pimpinan['id']; ?>' ><?php echo $pimpinan['nama']; ?><option>
                        <?php } ?>
                      </select>
                  </div> -->
                  <div class="col-md-12">
                      <button id='filter' class="btn btn-primary">Pilih</button>
                  </div>
                </div>
              
                <table id="custom_datatable" class="display table-hover dt-responsive" width="100%">
                  <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Waktu</th>
                        <th>Nama</th>
                        <th>Tempat</th>
                        <th>Keterangan</th>
                        <th>Pimpinan</th>
                    </tr>
                  </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  // d.id_pimpinan = $('#id_pimpinan option:selected').val()
  var table = $('#custom_datatable').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    "processing": true,
    "serverSide": true,
    "order": [],
    'autoWidth': false,
    'columns' : [
        { 'width' : '5%' },  
        { 'width' : '10%' },
        { 'width' : '10%' },
        { 'width' : '25%' },
        { 'width' : '20%' },
        { 'width' : '20%' }, 
        { 'width' : '10%' },    
    ],
    "ajax": {
      "url": "<?php echo site_url('user_jadwal/get_data_jadwal_json')?>",
       'data': function(d){
          d.waktu = $('#waktu').val()
          
        },
      "type": "POST"

    },


  });
  $("#filter").click(function(){
    console.log($('#waktu').val());
    //console.log($('#id_pimpinan option:selected').val());
    $('#custom_datatable').DataTable().ajax.reload();
  })

});

</script>
