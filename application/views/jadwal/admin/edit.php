<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ubah Jadwal</h3>
            </div>
			<?php echo form_open('admin_jadwal/edit/'.$jadwal['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
						<div class="col-md-12">
							<label for="nama" class="control-label"><span class="text-danger">*</span>Nama Acara</label>
							<div class="form-group">
								<input type="text" name="nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $jadwal['nama']); ?>" class="form-control" id="nama" />
								<span class="text-danger"><?php echo form_error('nama');?></span>
							</div>
						</div>
						<div class="col-md-12">
							<label for="waktu" class="control-label"><span class="text-danger">*</span>waktu</label>
							<div class="form-group">
								<input type="datetime-local" name="waktu" value="<?php echo date('Y-m-d\TH:i:s',strtotime($jadwal['waktu'])); ?>" class="form-control" id="waktu" />
								<span class="text-danger"><?php echo form_error('waktu');?></span>
							</div>
						</div>
			
						<div class="col-md-12">
							<label for="tempat" class="control-label"><span class="text-danger">*</span>Tempat</label>
							<div class="form-group">
								<input type="text" name="tempat" value="<?php echo ($this->input->post('tempat') ? $this->input->post('tempat') : $jadwal['tempat']); ?>" class="form-control" id="tempat" />
								<span class="text-danger"><?php echo form_error('tempat');?></span>
							</div>
						</div>
						<div class="col-md-12">
							<label for="pimpinan" class="control-label"><span class="text-danger">*</span>Pimpinan</label>
							<div class="form-group">
								<?php foreach($all_pimpinan as $pimpinan){ ?>
									<div class="checkbox">
										<label><input type="checkbox"  <?php echo in_array($pimpinan['userid'], $array_pimpinan_selected)?'checked':''; ?> name='pimpinan[]' value="<?php echo $pimpinan['userid']; ?>"><?php echo $pimpinan['nama']; ?></label>
									</div>
								<?php } ?> 
								<span class="text-danger"><?php echo form_error('pimpinan[]');?></span>
							</div>
						</div>
						<div class="col-md-12">
							<label for="user" class="control-label"><span class="text-danger">*</span>OPD Terkait</label>
							<div class="form-group">
								<?php foreach($all_user as $user){ ?>
									<div class="checkbox">
										<label><input type="checkbox" <?php echo in_array($user['userid'], $array_user_selected)?'checked':''; ?> name='user[]' value="<?php echo $user['userid']; ?>"><?php echo $user['nama']; ?></label>
									</div>
								<?php } ?> 
								<span class="text-danger"><?php echo form_error('user[]');?></span>
							</div>
						</div>`
						<div class="col-md-12">
							<label for="keterangan" class="control-label"><span class="text-danger">*</span>Keterangan</label>
							<div class="form-group">
								<textarea type="text" name="keterangan" class="form-control" id="keterangan"><?php echo ($this->input->post('keterangan') ? $this->input->post('keterangan') : $jadwal['keterangan']); ?></textarea>
								<span class="text-danger"><?php echo form_error('keterangan');?></span>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Simpan
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>