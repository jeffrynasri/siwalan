<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">User</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('user/add'); ?>" class="btn btn-success btn-sm">Tambah</a> 
                </div>
            </div>
            <div class="box-body">
                <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                  <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Deskripsi</th>
                        <th>Akun</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
                                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    "processing": true,
    "serverSide": true,
    "order": [],

    "ajax": {
      "url": "<?php echo site_url('user/get_data_user_json')?>",
      "type": "POST"

    },
    "columnDefs": [
      {
        "targets": [ 0 ],
        "orderable": false,
      },
    ],


  });

});

</script>
