<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Ubah User</h3>
      </div>
      <?php echo form_open_multipart('user/edit/'.$user['userid']); ?>
      <div class="box-body">
        <div class="row clearfix">
          <div class="col-md-12">
            <label for="nama" class="control-label"><span class='text-danger'>*</span>Nama</label>
            <div class="form-group">
              <input type="text" name="nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $user['nama']); ?>" class="form-control" id="nama" />
            </div>
          </div>
          <div class="col-md-12">
            <label for="uname" class="control-label"><span class="text-danger">*</span>Username</label>
            <div class="form-group">
            <input type="hidden" name="uname_old" value="<?php echo ($this->input->post('uname_old') ? $this->input->post('uname_old') : $user['uname']); ?>" class="form-control" id="uname_old" />
              <input type="text" name="uname" value="<?php echo ($this->input->post('uname') ? $this->input->post('uname') : $user['uname']); ?>" class="form-control" id="uname" />
              <span class="text-danger"><?php echo form_error('uname');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="pass" class="control-label">Password</label>
            <div class="form-group">
              <input type="password" name="pass" value="" class="form-control" id="pass" />
              <span class="text-danger"><?php echo form_error('pass');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="email" class="control-label">Email</label>
            <div class="form-group">
              <input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $user['email']); ?>" class="form-control" id="email" />
              <span class="text-danger"><?php echo form_error('email');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="deskripsi" class="control-label">Deskripsi</label>
            <div class="form-group">
              <textarea name="deskripsi" class="form-control" id="deskripsi" ><?php echo ($this->input->post('deskripsi') ? $this->input->post('deskripsi') : $user['deskripsi']); ?></textarea>
              <span class="text-danger"><?php echo form_error('deskripsi');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="ustate" class="control-label"><span class="text-danger">*</span>Status</label>
            <div class="form-group">
              <select name="ustate" class="form-control">
                <option value="1">Aktif</option>
                <?php if($user['ustate']=='0'){ ?>
                  <option value="0" selected >Tidak Aktif</option>
                <?php }else{ ?>
                  <option value="0" >Tidak Aktif</option>
                  <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('ustate');?></span>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-success">
          <i class="fa fa-check"></i> Simpan
        </button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
