<?php
	if($this->session->flashdata('pesan')){
		echo "<script> M.toast({html: '".$this->session->flashdata('pesan')."'}) </script>";
	};
?>


<div class="valign-wrapper" style="width:100%;height:80%;position: absolute;">
	<div class="valign" style="width:100%;">
		<div class="container">
			<div class="row">
				<div class="center-align col s12 ">
				<img style="margin-top: 50px;" class="responsive-img" width="15%" height="15%" src="<?php echo base_url();?>resources/images/gresikkab_logo.png"/>
					<img style="margin-top: 50px;" class="responsive-img" width="15%" height="15%" src="<?php echo base_url();?>resources/images/kominfogresik.jpg"/>
					<h5><?php echo APP_NAME; ?></h5>
					<p><?php echo APP_DESCRIPTION; ?></p>
				</div>
				<div class="col s12 m6 offset-m3">
					<div class="card">
						<div class="card-content ">
							<div class="row ">
								<?php echo form_open('authen/index');?>
									<div class="col s12">
										<div class="input-field col s12">
											<input id="uname" name="uname" type="text" class="validate" value="sekpri_bupati1">
											<label for="uname">User Name</label>
											<span class="red-text left"><?php echo form_error('uname');?></span>
										</div>
									</div>

									<div class="col s12">
										<div class="input-field col s12">
											<input id="password" name="password" type="password" class="validate" value="rotikeju">
											<label for="password">Password</label>
											<span class="red-text left"><?php echo form_error('password');?></span>
											<!-- <a href='authen/forgot_password' class="right">Lupa Password?</a> -->
										</div>
									</div>
                                    <div class='col s12'>
                                        <div class="input-field col s12">
                                            <p>
                                              <label>
                                                <input type="checkbox" class="filled-in" onclick='tampilPassword()' />
                                                <span>Tampilkan Password</span>
                                              </label>
                                            </p>
                                        </div>
                                    </div>
									<div class="col s12">
										<button type="submit" class="right waves-effect waves-light btn-small"><i class="material-icons right">arrow_forward</i>Login</button>
									</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
					<!-- <p class="center-align"><a href='authen/register'>Buat Akun</a></p> -->
                    <!-- <p class="center-align"><a target='_blank' href='<?php echo site_url('resources/images/diagrampenggunaan2.png'); ?>'>Diagram Penggunaan</a> | <a target='_blank' href='<?php echo site_url('resources/pdf/userguide_salak.pdf'); ?>'>Panduan Penggunaan Aplikasi <?php echo APP_NAME;?></a></p> -->
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    function tampilPassword(){
        var x = document.getElementById('password');
        if(x.type==='password'){
            x.type='text';
        }else{
            x.type='password';
        }
    }
	
</script>
