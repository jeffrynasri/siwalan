<style>
	.marginbottom {
	  margin-bottom: 6px;
	}
</style>
<!-- DataTable 1.10.19-->
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/material.min.css" defer>
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery.dataTables.min.css" defer>
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/responsive.dataTables.css" defer>
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/buttons.dataTables.min.css" defer>
<script src="<?php echo base_url();?>resources/js/jquery.dataTables.min.js" defer></script>
<script src="<?php echo base_url();?>resources/js/dataTables.responsive.js" type="text/javascript" defer></script>

<div class="row">
	<div class="col s12">
		<a href="<?php echo site_url('beranda/index'); ?>" class="left btn waves-effect waves-light">Beranda<i class="material-icons right">home</i></a>
	</div>
	<?php $this->load->view('home/papan_informasi'); ?>
	
	<div class="col s12">
		<h3 class="center" id='judul'>Data Antrian Tanggal <?php echo $tanggalsekarang;?></h3>
	</div>
	<div class="input-field col s3">
		<select name="tempat_vaksin" id="tempat_vaksin">
			<?php foreach($tempat_vaksin as $teva){ ?>
				<option value='<?php echo $teva['id']; ?>'><?php echo $teva['nama']; ?></option>
			<?php } ?>
		</select>		
	</div>
	<div class="input-field col s3">
		<input  value="<?php echo $tanggalplaceholder; ?>" name="waktu" id="waktu" type="date" class="validate">
		<label for="waktu">Tanggal</label>
	</div>
	<div class="col s3">
		<button id='filter' style="margin-top:12px;" class=" btn waves-effect waves-light" type="submit" name="action">Pilih</button>
	</div>
	<div class="col s12" >
			<table id="datatable" class="display table-hover dt-responsive" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Status</th>
					</tr>
				</thead>
			</table>
	</div>
</div>

<script>
  $(function () {
	document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
    $('#datatable').DataTable({ 
      dom: 'Bfrtip',      
	  "bLengthChange": false,
	   "pageLength": 10,
      "language": {
        "lengthMenu": "Tampilkan _MENU_ Data per Halaman",
        "zeroRecords": "Data Tidak Ditemukan.",
        "info": "Halaman Ke- _PAGE_ Dari _PAGES_",
        "infoEmpty": "Halaman Ke- 0 Dari 0",
        "infoFiltered": "(terfilter dari _MAX_ data)",
        "loadingRecords": "Mohon Tunggu...",
        "processing": "Sedang Diproses...",
        "search": "Cari:",
        "bSort": true,
        "pageLength":5,
        "paginate": {
          "previous": "Sebelumnya",
          "next": "Selanjutnya"
        }

      },
		aLengthMenu: [
			[10, 20, 50, -1],
			[10, 20, 50, "All"]
		],
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": "<?php echo site_url('beranda/get_data_daftar_vaksin_json')?>",
			'data': function(d){
				d.waktu = $('#waktu').val(),
				d.tempat_vaksin = $('#tempat_vaksin').val()
			},
			"type": "POST",
			"dataSrc": function ( json ) {
				//Make your callback here.
				var mydate = new Date($('#waktu').val());
				$('#judul').text('Data Antrian Tanggal ' + mydate.getDate()  + "-" + (mydate.getMonth()+1) + "-" + mydate.getFullYear())
				return json.data;
			}       
		},
		
    });
	$("#filter").click(function(){
		console.log($('#waktu').val());
		$('#datatable').DataTable().ajax.reload();
	})
	$(document).ready(function(){
		$('#tempat_vaksin').formSelect();
        $('#datatable').DataTable().ajax.reload();
    });
  })
</script>