<?php
	if($this->session->flashdata('pesan')){
		echo "<script> M.toast({html: '".$this->session->flashdata('pesan')."'}) </script>";
	};
?>

 <!-- Modal Structure -->
 <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Pengumuman</h4>
      <!-- <p>Untuk Saat Ini Jadwal Vaksinisasi WEP Sudah Penuh</p> -->
      <?php echo $setting['pengumuman']; ?>
      <?php echo $setting['narasi_jadwal']; ?>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Tutup</a>
    </div>
  </div>

<div class="container">
    <div class='row'>
        <div class="col s12">
            <a href="<?php echo site_url('authen/index'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">Login<i class="material-icons right">login</i></a>
            <a href="<?php echo site_url('beranda/unduh_pendaftaran'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">Bukti Pendaftaran<i class="material-icons right">download</i></a>
            <a href="<?php echo site_url('beranda/cek_pendaftaran'); ?>" class="right btn waves-effect waves-light">Cek Pendaftar<i class="material-icons right">search</i></a>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                <span class="card-title">Pendaftar Vaksin Hari Ini (<?php echo date("d/m/Y",strtotime($tanggalsekarang)); ?>)</span>
                <h2 class='center'><?php echo count($permohonan_vaksin_hariini); ?> Pendaftar</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php if(isset($nik)){ ?>
            <div class="col s12">
                
                <div class="card white darken-1">
                    <div class="card-content">
                        <span class="card-title">Pendaftaran :</span>
                        <p class="center text-center">Nik: <b><?php echo $nik; ?></b> </p>
                        <p class="center text-center">Nama: <b><?php echo $nama; ?></b> </p>
                        <p class="center text-center"><b>BERHASIL MELAKUKAN PENDAFTARAN</b> klik menu Bukti Pendaftaran untuk mengunduh bukti pendaftaran </p>
                    </div>
                    <div class="card-action">
                        <p class="center text-center">*Membawa <b><?php echo $setting['narasi_persyaratan_administrasi']; ?></b> Saat Hadir</p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php $this->load->view('home/papan_informasi'); ?>
        <div class="col s12">
            <h3 class="center text-center">PENDAFTARAN VAKSINASI KABUPATEN GRESIK</h3>
        </div>
        <?php if($setting['is_pendaftaran_open']=='1' && $isthereopenslot=='1') {?>
            <form class="col s12" method="post" action="<?php echo site_url('beranda/index'); ?>">
                <div class="row">
                    <label style="margin-left:8px;">Tempat Vaksin</label>
                    <div class="input-field col s12">
                        <select name="tempat_vaksin" id="tempat_vaksin">
                            <?php foreach($list_tempatvaksin_open as $teva){ ?>
                                <option value='<?php echo $teva['id_tempat_vaksin']; ?>'><?php echo $teva['tempat_vaksin_nama']; ?></option>
                            <?php } ?>
                        </select>
                        <?php if(!empty($list_tempatvaksin_penuh['error'])){?>
                            <span class="helper-text" data-error="wrong" data-success="right"><?php echo $list_tempatvaksin_penuh['error'];?></span>
                        <?php }?>
                        <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('tempat_vaksin');?></span>
                    </div>
                </div>
      
                <div class="row">
                    <label style="margin-left:8px;">Tanggal Vaksin<span id='tulisan_loading'> (Memuat Jadwal Vaksin...)</span></label>
                    <div class="input-field col s12">
                        <select name="waktu" id="waktu">
                        </select>
                        <span id='waktu_error' class="helper-text" data-error="wrong" data-success="right"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input value="<?php echo ($this->input->post('nik') ? $this->input->post('nik') : ""); ?>" name="nik" id="nik" type="number" class="validate">
                    <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('nik');?></span>
                    <label for="nik">NIK (Wajib diisi)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input  value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : ""); ?>" name="nama" id="nama" type="text" class="validate">
                    <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('nama');?></span>
                    <label for="nama">Nama (Wajib diisi)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input  value="<?php echo ($this->input->post('no_hp') ? $this->input->post('no_hp') : ""); ?>" name="no_hp" id="no_hp" type="number" class="validate">
                    <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('no_hp');?></span>
                    <label for="no_hp">No HP (Wajib diisi)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input  value="<?php echo ($this->input->post('alamat') ? $this->input->post('alamat') : ""); ?>" name="alamat" id="alamat" type="text" class="validate">
                    <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('alamat');?></span>
                    <label for="alamat">Alamat (Wajib diisi)</label>
                    </div>
                </div>
                <button id='kirim' <?php echo $setting['is_pendaftaran_open']==0?'disabled':""; ?> class="right btn waves-effect waves-light" type="submit" name="action" onclick='return confirm(" Apakah Data yang Dikirim sudah Benar ?")'>Kirim
                    <i class="material-icons right">send</i>
                </button>
            </form>
        <?php }else{ ?>
            <h4 class='center'>Kapasitas Vaksin sudah penuh. <b>Pendaftaran Sementara Ditutup.</b></h4>
        <?php } ?>
    </div>
</div>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
    // Or with jQuery
    function get_json_jadwal(){
        //Validate fields if required using jQuery        
        var postForm = { //Fetch form data
            'id_tempat_vaksin'     : $('#tempat_vaksin option:selected').val()
        };
        
        $.ajax({ //Process the form using $.ajax()
            type      : 'POST', //Method type
            url       : '<?php echo site_url("beranda/finding_slot_vaksin_json"); ?>', //Your form processing file URL
            data      : postForm, //Forms name
            dataType  : 'json',
            success   : function(data) {
                        //console.log(data)    
                        if(data['isthereopenslot']=='1'){
                            $('#kirim').attr('disabled',true);
                            data['list_tanggalvaksin_open'].forEach(function(item,index){
                                var tanggal = new Date(item['tanggal']);
                                var isi_option=('0' + tanggal.getDate()).slice(-2) + '-' + ('0' +(tanggal.getMonth()+1)).slice(-2) + '-' + tanggal.getFullYear()+ " tersedia " + item['sisa'] + " vaksin"
                                $("#waktu").append($("<option></option>").attr("value",item['tanggal']).text(isi_option));
                            })
                            $('#waktu_error').text(data['string_tanggalvaksin_penuh']);
                            $('#kirim').removeAttr('disabled');
                        } else{
                            $('#waktu_error').text(data['string_tanggalvaksin_penuh']);
                            $('#kirim').attr('disabled',true);
                            M.toast({html: 'Kuota Vaksin Penuh'})
                        }
                        $('#waktu').formSelect();
                        $('#tulisan_loading').hide();
                       
            }
        });
     //   event.preventDefault(); //Prevent the default submit
    }
    function init_jadwal(){
        $('#tulisan_loading').show();         
        $("#waktu").empty().html(' '); 
        $('#waktu').formSelect();
        get_json_jadwal()
    }
    $(document).ready(function(){
        <?php if($setting['is_popup_active']==1){ ?>
            $('.modal').modal();
            //now you can open modal from code
            $('#modal1').modal('open');

            //or by click on trigger
            $('.trigger-modal').modal();
        <?php } ?>
        init_jadwal()
    });

    $('#tempat_vaksin').on('change', function() {
        //console.log( this.value );
        init_jadwal();
    });
  </script>