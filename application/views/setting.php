<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ubah pengaturan</h3>
				<!-- <p><span class="text-danger">*</span>Untuk Ubah Pengaturan Hubungi Dinas Kominfo</p> -->
            </div>
			<?php echo form_open('setting/index'); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-12">
						<label for="limit_vaksin" class="control-label"><span class="text-danger">*</span>Limit Vaksin</label>
						<div class="form-group">
							<input type="number" name="limit_vaksin" value="<?php echo ($this->input->post('limit_vaksin') ? $this->input->post('limit_vaksin') : $setting['limit_vaksin']); ?>" class="form-control" id="limit_vaksin" />
							<span class="text-danger"><?php echo form_error('limit_vaksin');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="narasi_jadwal" class="control-label"><span class="text-danger">*</span>Narasi Jadwal</label>
						<div class="form-group">
							<textarea name="narasi_jadwal" class="form-control" id="narasi_jadwal"><?php echo ($this->input->post('narasi_jadwal') ? $this->input->post('narasi_jadwal') : $setting['narasi_jadwal']); ?></textarea>
							<span class="text-danger"><?php echo form_error('narasi_jadwal');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="narasi_persyaratan_administrasi" class="control-label"><span class="text-danger">*</span>Narasi Persyaratan Administrasi</label>
						<div class="form-group">
							<textarea name="narasi_persyaratan_administrasi" class="form-control" id="narasi_persyaratan_administrasi"><?php echo ($this->input->post('narasi_persyaratan_administrasi') ? $this->input->post('narasi_persyaratan_administrasi') : $setting['narasi_persyaratan_administrasi']); ?></textarea>
							<span class="text-danger"><?php echo form_error('narasi_persyaratan_administrasi');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="pengumuman" class="control-label"><span class="text-danger">*</span>Pengumuman</label>
						<div class="form-group">
							<textarea name="pengumuman" class="form-control" id="pengumuman"><?php echo ($this->input->post('pengumuman') ? $this->input->post('pengumuman') : $setting['pengumuman']); ?></textarea>
							<span class="text-danger"><?php echo form_error('pengumuman');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="rentang_vaksin_awal" class="control-label"><span class="text-danger">*</span>Vaksin Awal</label>
						<div class="form-group">
							<input type="date" name="rentang_vaksin_awal" value="<?php echo ($this->input->post('rentang_vaksin_awal') ? $this->input->post('rentang_vaksin_awal') : date('Y-m-d',strtotime($setting['rentang_vaksin_awal']))); ?>" class="form-control" id="rentang_vaksin_awal" />
							<span class="text-danger"><?php echo form_error('rentang_vaksin_awal');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="rentang_vaksin_akhir" class="control-label"><span class="text-danger">*</span>Vaksin Akhir</label>
						<div class="form-group">
							<input type="date" name="rentang_vaksin_akhir" value="<?php echo ($this->input->post('rentang_vaksin_akhir') ? $this->input->post('rentang_vaksin_akhir') : date('Y-m-d',strtotime($setting['rentang_vaksin_akhir']))); ?>" class="form-control" id="rentang_vaksin_akhir" />
							<span class="text-danger"><?php echo form_error('rentang_vaksin_akhir');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="password_dewa" class="control-label"><span class="text-danger">*</span>Password</label>
						<div class="form-group">
							<input type="password" name="password_dewa" class="form-control" id="password_dewa" />
							<span class="text-danger"><?php echo form_error('password_dewa');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Simpan
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>