<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="<?php if($this->uri->segment(1,0)=='jadwal' ){echo 'active';}else{echo '';} ?>">
          <a href="<?php echo site_url('jadwal/index'); ?>"><i class="fa fa-calendar-check-o"></i><span>Jadwal</span></a>
        </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
