<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title><?php echo APP_NAME; ?></title>
	<!-- ICON -->
	<link rel='icon' href="<?php echo site_url('resources/images/gresikkab_logo.png');?>"/>
  <style>
     body {
         display: flex;
         min-height: 100vh;
         flex-direction: column;
     }
     main {
         flex: 1 0 auto;
     }
  </style>
  
	<!-- Jquery 3.3.1 -->
	<script src="<?php echo base_url();?>resources/js/jquery-3.3.1.js" ></script>
  
	<!-- Materialize -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>resources/css/materialize.min.css"  media="screen,projection" defer/>
	<script src="<?php echo base_url();?>resources/js/materialize.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons&display=swap" rel="stylesheet" defer>

</head>
<body>
  <?php $this->load->view('layouts/home_header'); ?>
  <main>
    <?php
    if(isset($_view) && $_view) $this->load->view($_view);
    ?>
  </main>
  <?php $this->load->view('layouts/home_footer'); ?>
</body>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-202800716-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->

</html>
