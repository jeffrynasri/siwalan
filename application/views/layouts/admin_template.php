<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo APP_NAME; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- jQuery 2.2.3 -->
	<script src="<?php echo site_url('resources/adminlte/js/jquery-2.2.3.min.js');?>"></script>
	<!-- ICON-->
	<link rel='icon' href="<?php echo site_url('resources/images/gresikkab_logo.png');?>"/>
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo site_url('resources/adminlte/css/bootstrap.min.css');?>">
	<script src="<?php echo site_url('resources/adminlte/js/bootstrap.min.js');?>"></script>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo site_url('resources/adminlte/css/font-awesome.min.css');?>">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Datetimepicker -->
	<link rel="stylesheet" href="<?php echo site_url('resources/adminlte/css/bootstrap-datetimepicker.min.css');?>">
	<!-- FastClick -->
	<script src="<?php echo site_url('resources/adminlte/js/fastclick.js');?>"></script>
	<!-- DataTable 1.10.19-->
	<link rel="stylesheet" href="<?php echo site_url('resources/css/jquery.dataTables.min.css');?>">
	<link rel="stylesheet" href="<?php echo site_url('resources/css/responsive.dataTables.css');?>">
	<script src="<?php echo site_url('resources/js/jquery.dataTables.min.js');?>"></script>
	<script src="<?php echo site_url('resources/js/dataTables.responsive.js');?>" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/buttons.dataTables.min.css">
	<script src="<?php echo base_url();?>resources/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/dataTables.responsive.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>resources/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/jszip.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/pdfmake.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/vfs_fonts.js"></script>
	<script src="<?php echo base_url();?>resources/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/buttons.print.min.js"></script>
	<!-- Admin LTE -->
	<link rel="stylesheet" href="<?php echo site_url('resources/adminlte/css/AdminLTE.min.css');?>">
	<link rel="stylesheet" href="<?php echo site_url('resources/adminlte/css/_all-skins.min.css');?>">
	<script src="<?php echo site_url('resources/adminlte/js/app.min.js');?>"></script>
	<script src="<?php echo site_url('resources/adminlte/js/demo.js');?>"></script>
	<!-- DatePicker -->
	<script src="<?php echo site_url('resources/adminlte/js/moment.js');?>"></script>
	<script src="<?php echo site_url('resources/adminlte/js/bootstrap-datetimepicker.min.js');?>"></script>
	<script src="<?php echo site_url('resources/adminlte/js/global.js');?>"></script>

	<!-- Highcart -->
	<script src="<?php echo base_url().'resources/js/highcharts.js'?>"></script>
	<script src="<?php echo base_url().'resources/js/series-label.js'?>"></script>
	<script src="<?php echo base_url().'resources/js/exporting.js'?>"></script>

	<script src="<?php echo base_url().'resources/js/filterDropDown.min.js'?>"></script>
	<style>
		table.dataTable tbody td {
			word-break: break-word;
			vertical-align: top;
		}
	</style>

</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
        <?php $this->load->view($_header); ?>
		<?php $this->load->view($_sidebar); ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<?php
				if(isset($_view) && $_view)
				$this->load->view($_view);
				?>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			© 2021 Copyright <a target="_blank" href='#'>Dinas Komunikasi dan Informatika Kab Gresik</a>
			<a class="black-text text-lighten-4 right pull-right right" href="#!">Page rendered in <strong>{elapsed_time}</strong> seconds</a>
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">

			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<!-- Home tab content -->
				<div class="tab-pane" id="control-sidebar-home-tab">

				</div>
				<!-- /.tab-pane -->
				<!-- Stats tab content -->
				<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
				<!-- /.tab-pane -->

			</div>
		</aside>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
		immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
</html>
