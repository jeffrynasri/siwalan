<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <!-- <li class="header">Kriteria</li> -->
        <li class="<?php if($this->uri->segment(1,0)=='admin_dashboard' ){echo 'active';}else{echo '';} ?>">
          <a href="<?php echo site_url('admin_dashboard/index'); ?>"><i class="fa fa-pie-chart"></i><span>Dashboard</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='admin_jadwal' ){echo 'active';}else{echo '';} ?>">
          <a href="<?php echo site_url('admin_jadwal/index'); ?>"><i class="fa fa-calendar-check-o"></i><span>Jadwal</span></a>
        </li>
        <li class="header">Pengguna</li>
        <li class="<?php if($this->uri->segment(1,0)=='pimpinan'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('pimpinan/index') ?>"><i class="fa fa-cog"></i><span>Pimpinan</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='user'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('user/index') ?>"><i class="fa fa-user"></i><span>OPD</span></a>
        </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
