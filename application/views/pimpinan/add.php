<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Pimpinan</h3>
      </div>	

      <?php echo form_open_multipart('pimpinan/add'); ?>
      <div class="box-body">
        <div class="row clearfix">
          <div class="col-md-12">
            <label for="nama" class="control-label"><span class='text-danger'>*</span>Nama</label>
            <div class="form-group">
              <input type="text" name="nama" value="<?php echo $this->input->post('nama'); ?>" class="form-control" id="nama" />
            </div>
          </div>
          <div class="col-md-12">
            <label for="uname" class="control-label"><span class="text-danger">*</span>Username</label>
            <div class="form-group">
              <input type="text" name="uname" value="<?php echo $this->input->post('uname'); ?>" class="form-control" id="uname" />
              <span class="text-danger"><?php echo form_error('uname');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="pass" class="control-label"><span class="text-danger">*</span>Password</label>
            <div class="form-group">
              <input type="password" name="pass" value="<?php echo $this->input->post('pass'); ?>" class="form-control" id="pass" />
              <span class="text-danger"><?php echo form_error('pass');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="email" class="control-label">Email</label>
            <div class="form-group">
              <input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
              <span class="text-danger"><?php echo form_error('email');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="deskripsi" class="control-label">Deskripsi</label>
            <div class="form-group">
              <textarea  name="deskripsi" value="<?php echo $this->input->post('deskripsi'); ?>" class="form-control" id="deskripsi" ></textarea>
              <span class="text-danger"><?php echo form_error('deskripsi');?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="ustate" class="control-label">Deskripsi</label>
            <div class="form-group">
              <select name="ustate" id="ustate" class="form-control">
                <option value="1">Aktif</option>
                <option value="0">Tidak Aktif</option>
              </select>
              <span class="text-danger"><?php echo form_error('ustate');?></span>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-success">
          <i class="fa fa-check"></i> Simpan
        </button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
