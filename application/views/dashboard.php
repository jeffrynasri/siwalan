<style>
  table.table-bordered{
    border:1px solid black;
    margin-top:20px;
  }
  table.table-bordered > thead > tr > th{
      border:1px solid black;
  }
  table.table-bordered > tbody > tr > td{
      border:1px solid black;
  }
   table {border-collapse:collapse; table-layout:fixed; width:100%;}
   table td {border:solid 1px #fab; width:100px; word-wrap:break-word;}
   </style>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                Dashboard
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info btn-info">
                        <div class="inner">
                            <h3><?php echo count($all_jadwal); ?></h3>
                            <p>Kegiatan</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-group"></i>
                        </div>
                        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                    <!-- ./col -->
                    </div>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Hari Ini</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Minggu Ini</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">Semua</a></li>
        
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <p>Tanggal : <b> <?php echo date('D, d-m-Y'); ?> </b></p>
                      <table class="table table-bordered" width="100%">
                        <thead>
                          <tr>
                              <th style="width :10%">Waktu</th>
                              <th style="width :30%">Nama Kegiatan</th>
                              <th style="width :30%">Tempat</th>
                              <th style="width :30%">Keterangan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($jadwal_hariini as $jad_hariini){ ?>
                              <tr>
                                <td><?php echo date('H:i', strtotime($jad_hariini['waktu'])); ?></td>
                                <td><?php echo $jad_hariini['nama']; ?></td>
                                <td><?php echo $jad_hariini['tempat']; ?></td>
                                <td><?php echo $jad_hariini['keterangan']; ?></td>
                              </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                      <table class='table table-bordered' width='100%'>
                        <?php foreach($jadwal_mingguini as $key => $value){ ?>
                          <thead>
                            <tr>
                              <th style="width :100%"colspan="4"><b><?php echo $key; ?></b> <span class="label label-success"><?php echo count($value) . " Kegiatan" ?></span></th>
                            </tr>
                            <tr>
                              <th style="width :10%">Waktu</th>
                              <th style="width :30%">Nama Kegiatan</th>
                              <th style="width :30%">Tempat</th>
                              <th style="width :30%">Keterangan</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach($value as $jad_mingguini){ ?>
                              <tr>
                                <td><?php echo date('H:i', strtotime($jad_mingguini['waktu'])); ?></td>
                                <td><?php echo $jad_mingguini['nama']; ?></td>
                                <td><?php echo $jad_mingguini['tempat']; ?></td>
                                <td><?php echo $jad_mingguini['keterangan']; ?></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        <?php } ?>
                      </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                      <p>*Catatan : Pencarian dengan NIK/NAMA/NO_HP/ALAMAT</p>
                      <table id="custom_datatable" class="display table-hover dt-responsive" width="100%">
                        <thead>
                          <tr>
                              <th>No</th>
                              <th>Tanggal</th>
                              <th>Waktu</th>
                              <th>Nama</th>
                              <th>Tempat</th>
                              <th>Pimpinan</th>
                              <th>OPD Terkait</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
              </div>
      
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
    "processing": true,
    "serverSide": true,
    "order": [],
    "columnDefs": [
      {
        "targets": [ 1,2,5,6 ],
        "orderable": false,
      },
    ],
    'autoWidth': false,
    'columns' : [
        { 'width' : '5%' },  
        { 'width' : '10%' },
        { 'width' : '10%' },
        { 'width' : '20%' },
        { 'width' : '20%' },
        { 'width' : '20%' },
        { 'width' : '15%' },   
    ],
    "ajax": {
      "url": "<?php echo site_url('admin_dashboard/get_data_jadwal_json')?>",
      "type": "POST"

    }


  });

});

</script>
