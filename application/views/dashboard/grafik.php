<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                Grafik
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php echo form_open('dashboard/grafik') ?>
                        <div class="col-md-6">
                            <span>Dari</span>
                            <input type="date" name="waktu_dari" id="waktu_dari" value="<?php echo $this->input->post('waktu_dari')?$this->input->post('waktu_dari'):date('Y-m-d'); ?>"/>
                        </div>
                        <div class="col-md-12">
                            <span>Sampai</span>
                            <input type="date" name="waktu_sampai" id="waktu_sampai" value="<?php echo $this->input->post('waktu_dari')?$this->input->post('waktu_dari'):date('Y-m-d'); ?>"/>
                        </div>
                        <div class="col-md-12">
                            <span>Lokasi</span>
                            <select name='id_tempat_vaksin' id="id_tempat_vaksin">
                                <option value='-1' selected>Semua<option>
                                <?php foreach($tempat_vaksin as $teva){ ?>
                                <option value='<?php echo $teva['id']; ?>' ><?php echo $teva['nama']; ?><option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <button id='filter' type='submit' class="btn btn-primary">Pilih</button>
                        </div>
                    <?php echo form_close() ?>
                </div>
            </div>
            <div class="box-body">
                <div id="container"></div>
                <table class="table" width="100%">
                  <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Hadir</th>
                        <th>Tidak Hadir</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php for($i=0;$i<count($xasix);$i++){ ?>
                        <tr>
                            <td><?php echo date($xasix[$i]); ?></td>
                            <td><?php echo $yasix[0]['data'][$i]; ?></td>
                            <td><?php echo $yasix[1]['data'][$i]; ?></td>
                        </tr>
                      <?php } ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Grafik Pendaftar Vaksin WEP Kab Gresik'
    },
    xAxis: {
        categories: <?php echo json_encode($xasix); ?>
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Pendaftar'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'gray'
            }
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        },
        column: {
            dataLabels: {
                formatter: function () {
                    return Math.round(100 * this.y / this.total) + '%';
                },
                enabled: true
            }
        }
    },
    series: <?php echo json_encode($yasix); ?>
});
  $("#filter").click(function(){
   // $('#custom_datatable').DataTable().ajax.reload();
  })

});

</script>