<?php

class Jadwal_model extends CI_Model
{
    // id	
    // nama	
    // tempat	
    // keterangan	
    // waktu	

    var $table = 'jadwal';
    var $column_order = array('jadwal.id','jadwal.nama','jadwal.tempat','jadwal.keterangan','jadwal.waktu'); 
    var $column_search = array('jadwal.id','jadwal.nama','jadwal.tempat','jadwal.keterangan','jadwal.waktu');
    var $order = array('jadwal.nama' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='jadwal.*',$order_by="nama")
    {
      $this->db->where($params);
      $this->db->order_by($order_by, 'asc');
      $this->db->select('jadwal.*');
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='jadwal.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'jadwal.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='jadwal.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'jadwal.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='jadwal.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get jadwal by id
     */
    function get_jadwal($id)
    {
        $this->db->select('jadwal.*');
        return $this->db->get_where('jadwal',array('jadwal.id'=>$id))->row_array();
    }
   
    /*
     * Get all jadwal
     */
    function get_all_jadwal($params=array(),$order_by="nama")
    {
        $this->db->order_by($order_by, 'asc');
        $this->db->select('jadwal.*');
        $this->db->where($params);
        return $this->db->get('jadwal')->result_array();
    }
    
    
    function add_jadwal_new($params)
    {
      return $this->db->insert('jadwal',$params);
    }
    
    /*
     * function to update jadwal
     */
    function update_jadwal($id,$params)
    {
        $this->db->where('jadwal.id',$id);
        return $this->db->update('jadwal',$params);
    }
    
    /*
     * function to delete jadwal
     */
    function delete_jadwal($id)
    {
        return $this->db->delete('jadwal',array('jadwal.id'=>$id));
    }

}
