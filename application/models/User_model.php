<?php

class User_model extends CI_Model
{
    // nama
    // email
    // uname
    // deskripsi
    // ustate
    var $table = 'user';

    var $column_order = array('user.nama','user.email','user.uname','user.deskripsi','user.ustate'); //set column field database for datatable orderable
    var $column_search = array('user.nama','user.email','user.uname','user.deskripsi','user.ustate');//set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('user.nama' => 'asc'); // default order
  
    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array())
    {
      $this->db->where($params);
      $this->db->select('user.*');
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
  
    function get_datatables($params=array())
    {
      $this->_get_datatables_query( $params?$params:array() );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array())
    {
      $this->_get_datatables_query($params);
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array())
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get user by user
     */
    function get_user($userid)
    {
        return $this->db->get_where('user',array('userid'=>$userid))->row_array();
    }
    /*
     * Get pimpinan by pimpinan
     */
    function get_user_byusername($username)
    {
      return $this->db->get_where('user',array('uname'=>$username))->row_array();
    }   
        
    /*
     * Get all user
     */
    function get_all_user()
    {
        $this->db->order_by('nama', 'asc');
        return $this->db->get('user')->result_array();
    }
        
    /*
     * function to add new user
     */
    function add_user($params)
    {
        return $this->db->insert('user',$params);
    }
    
    /*
     * function to update user
     */
    function update_user($userid,$params)
    {
        $this->db->where('userid',$userid);
        return $this->db->update('user',$params);
    }
    
    /*
     * function to delete user
     */
    function delete_user($userid)
    {
        return $this->db->delete('user',array('userid'=>$userid));
    }
}
