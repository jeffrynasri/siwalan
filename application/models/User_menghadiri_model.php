<?php

class User_menghadiri_model extends CI_Model
{
    // id_user	
    // id_jadwal

    var $table = 'user_menghadiri';
    var $column_order = array('user_menghadiri.id_user','user_menghadiri.id_jadwal'); 
    var $column_search = array('user_menghadiri.id_user','user_menghadiri.id_jadwal');
    var $order = array('user_menghadiri.id_user' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='user_menghadiri.*',$order_by="nama")
    {
      $this->db->where($params);
      //$this->db->order_by($order_by, 'asc');
      $this->db->select($select.',user.nama as user_nama,jadwal.*');
      $this->db->join('jadwal', 'user_menghadiri.id_jadwal = jadwal.id');
      $this->db->join('user', 'user_menghadiri.id_user = user.userid');
      $this->db->from($this->table);      
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='user_menghadiri.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'user_menghadiri.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='user_menghadiri.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'user_menghadiri.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='user_menghadiri.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->join('jadwal', 'user_menghadiri.id_jadwal = jadwal.id');
      $this->db->join('user', 'user_menghadiri.id_user = user.userid');
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get user_menghadiri by id
     */
    function get_user_menghadiri($where)
    {
        $this->db->select('user_menghadiri.*,user.nama as user_nama,jadwal.*');
        $this->db->join('jadwal', 'user_menghadiri.id_jadwal = jadwal.id');
        $this->db->join('user', 'user_menghadiri.id_user = user.userid');
        return $this->db->get_where($where)->result_array();
    }
   

    /*
     * Get all user_menghadiri
     */
    function get_all_user_menghadiri($params=array(),$order_by="nama")
    {
        //$this->db->order_by($order_by, 'asc');
        $this->db->select('user_menghadiri.*,user.nama as user_nama,jadwal.*');
        $this->db->join('jadwal', 'user_menghadiri.id_jadwal = jadwal.id');
        $this->db->join('user', 'user_menghadiri.id_user = user.userid');
        $this->db->where($params);
        return $this->db->get('user_menghadiri')->result_array();
    }
    /*
     * Get all user_menghadiri
     */
    function get_all_user_menghadiri_selectparam($select="user_menghadiri.*",$params=array(),$order_by="nama")
    {
        $this->db->order_by($order_by, 'asc');
        $this->db->select($select);
        $this->db->join('jadwal', 'user_menghadiri.id_jadwal = jadwal.id');
        $this->db->join('user', 'user_menghadiri.id_user = user.userid');
        $this->db->where($params);
        return $this->db->get('user_menghadiri')->result_array();
    }
        
    
    function add_user_menghadiri_new($params)
    {
      return $this->db->insert('user_menghadiri',$params);
    }
    
    /*
     * function to update user_menghadiri
     */
    function update_user_menghadiri($where,$params)
    {
        $this->db->where($where);
        return $this->db->update('user_menghadiri',$params);
    }
    
    /*
     * function to delete user_menghadiri
     */
    function delete_user_menghadiri($where)
    {
        return $this->db->delete('user_menghadiri',$where);
    }
}
