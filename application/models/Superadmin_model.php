<?php

class Superadmin_model extends CI_Model
{
    // nama
    // email
    // uname
    // deskripsi
    // ustate
    var $table = 'superadmin';

    var $column_order = array('superadmin.nama','superadmin.email','superadmin.uname','superadmin.deskripsi','superadmin.ustate'); //set column field database for datatable orderable
    var $column_search = array('superadmin.nama','superadmin.email','superadmin.uname','superadmin.deskripsi','superadmin.ustate');//set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('superadmin.nama' => 'asc'); // default order
  
    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array())
    {
      $this->db->where($params);
      $this->db->select('superadmin.*');
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
  
    function get_datatables($params=array())
    {
      $this->_get_datatables_query( $params?$params:array() );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array())
    {
      $this->_get_datatables_query($params);
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array())
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get superadmin by superadmin
     */
    function get_superadmin($userid)
    {
        return $this->db->get_where('superadmin',array('userid'=>$userid))->row_array();
    }
    /*
     * Get pimpinan by pimpinan
     */
    function get_superadmin_byusername($username)
    {
      return $this->db->get_where('superadmin',array('uname'=>$username))->row_array();
    }   
    /*
     * Get all superadmin
     */
    function get_all_superadmin()
    {
        $this->db->order_by('nama', 'asc');
        return $this->db->get('superadmin')->result_array();
    }
        
    /*
     * function to add new superadmin
     */
    function add_superadmin($params)
    {
        return $this->db->insert('superadmin',$params);
    }
    
    /*
     * function to update superadmin
     */
    function update_superadmin($superadminid,$params)
    {
        $this->db->where('superadminid',$superadminid);
        return $this->db->update('superadmin',$params);
    }
    
    /*
     * function to delete superadmin
     */
    function delete_superadmin($superadminid)
    {
        return $this->db->delete('superadmin',array('superadminid'=>$superadminid));
    }
}
