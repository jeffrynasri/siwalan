<?php

class Jadwal_pimpinan_model extends CI_Model
{
    // id_jadwal
    // id_pimpinan

    var $table = 'jadwal_pimpinan';
    var $column_order = array('jadwal_pimpinan.id_jadwal','jadwal_pimpinan.id_pimpinan'); 
    var $column_search = array('jadwal_pimpinan.id_jadwal','jadwal_pimpinan.id_pimpinan');
    var $order = array('jadwal_pimpinan.nama' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='jadwal_pimpinan.*',$order_by="nama")
    {
      $this->db->where($params);
      //$this->db->order_by($order_by, 'asc');
      $this->db->select($select.',pimpinan.nama as pimpinan_nama,jadwal.*');
      $this->db->join('jadwal', 'jadwal_pimpinan.id_jadwal = jadwal.id');
      $this->db->join('pimpinan', 'jadwal_pimpinan.id_pimpinan = pimpinan.userid');
      $this->db->from($this->table);      
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='jadwal_pimpinan.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'jadwal_pimpinan.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='jadwal_pimpinan.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'jadwal_pimpinan.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='jadwal_pimpinan.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->join('jadwal', 'jadwal_pimpinan.id_jadwal = jadwal.id');
      $this->db->join('pimpinan', 'jadwal_pimpinan.id_pimpinan = pimpinan.userid');
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get jadwal_pimpinan by id
     */
    function get_jadwal_pimpinan($where)
    {
        $this->db->select('jadwal_pimpinan.*,pimpinan.nama as pimpinan_nama,jadwal.*');
        $this->db->join('jadwal', 'jadwal_pimpinan.id_jadwal = jadwal.id');
        $this->db->join('pimpinan', 'jadwal_pimpinan.id_pimpinan = pimpinan.userid');
        return $this->db->get_where($where)->result_array();
    }
   

    /*
     * Get all jadwal_pimpinan
     */
    function get_all_jadwal_pimpinan($params=array(),$order_by="nama")
    {
        //$this->db->order_by($order_by, 'asc');
        $this->db->select('jadwal_pimpinan.*,pimpinan.nama as pimpinan_nama,jadwal.*');
        $this->db->join('jadwal', 'jadwal_pimpinan.id_jadwal = jadwal.id');
        $this->db->join('pimpinan', 'jadwal_pimpinan.id_pimpinan = pimpinan.userid');
        $this->db->where($params);
        return $this->db->get('jadwal_pimpinan')->result_array();
    }
    /*
     * Get all jadwal_pimpinan
     */
    function get_all_jadwal_pimpinan_selectparam($select="jadwal_pimpinan.*",$params=array(),$order_by="nama")
    {
        //$this->db->order_by($order_by, 'asc');
        $this->db->select($select);
        $this->db->join('jadwal', 'jadwal_pimpinan.id_jadwal = jadwal.id');
        $this->db->join('pimpinan', 'jadwal_pimpinan.id_pimpinan = pimpinan.userid');
        $this->db->where($params);
        return $this->db->get('jadwal_pimpinan')->result_array();
    }
        
    
    function add_jadwal_pimpinan_new($params)
    {
      return $this->db->insert('jadwal_pimpinan',$params);
    }
    
    /*
     * function to update jadwal_pimpinan
     */
    function update_jadwal_pimpinan($where,$params)
    {
        $this->db->where($where);
        return $this->db->update('jadwal_pimpinan',$params);
    }
    
    /*
     * function to delete jadwal_pimpinan
     */
    function delete_jadwal_pimpinan($where)
    {
        return $this->db->delete('jadwal_pimpinan',$where);
    }
}
